/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vectrongaming.portalcontrol;

import com.palmergames.bukkit.towny.Towny;
import com.palmergames.bukkit.towny.object.TownyUniverse;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author DanielRHarris
 */
public class PortalControl extends JavaPlugin {

    public static TownyUniverse tUniverse;
    public static double na, reset;
    
        @Override
    public void onDisable() {
        try {
            ControlManager.save(getDataFolder());
        } catch (Exception ex) {
            Logger.getLogger(PortalControl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onEnable() {
        try {
            ControlManager.load(getDataFolder());
        } catch (Exception ex) {
            Logger.getLogger(PortalControl.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Load data
        ControlManager.calculateScore();
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new ControlListener(this), this);
        getCommand("portalcontrol").setExecutor(new CommandExecuter(this));
        tUniverse = ((Towny) Bukkit.getPluginManager().getPlugin("Towny")).getTownyUniverse();

        getConfig().addDefault("reset", 7);
        getConfig().options().copyDefaults(true);
        saveConfig();

        reset = getConfig().getInt("reset");


    }
}

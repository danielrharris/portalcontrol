/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vectrongaming.portalcontrol;

import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Nation;
import com.palmergames.bukkit.towny.object.Town;
import com.palmergames.bukkit.towny.object.TownyUniverse;
import com.vectrongaming.portalcontrol.ControlManager.MutableInteger;
import org.bukkit.Location;
import org.bukkit.World.Environment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerPortalEvent;


/**
 *
 * @author DanielRHarris
 */
public class ControlListener implements Listener {

    ControlListener(PortalControl aThis) {
    }
    
   @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        EntityDamageEvent edc = event.getEntity().getLastDamageCause();
        if (!(edc instanceof EntityDamageByEntityEvent)) {
            return;
        }
        EntityDamageByEntityEvent edbee = (EntityDamageByEntityEvent) edc;
        if (!(edbee.getDamager() instanceof Player)) {
            return;
        }
        Player damager = (Player) edbee.getDamager();
        try {
            
            Town tdamagerr = TownyUniverse.getDataSource().getResident(damager.getName()).getTown();
            
            if (ControlManager.towns.containsKey(tdamagerr)) {
                ControlManager.towns.get(tdamagerr).value++;
                ControlManager.calculateScore();
            }
            else {
                ControlManager.towns.put(tdamagerr.toString(), new MutableInteger(1));
                ControlManager.calculateScore();
            }
        }

         catch (Exception ex) {
        }
        
   }
   
   public void onPlayerPortal(final PlayerPortalEvent event) {
        if(event.isCancelled()) {
       }
        Player player = event.getPlayer();
        Location from = event.getFrom();
        Location to = event.getTo();
        if(player == null || from == null || to == null) {
       }
        if(to.getWorld().getEnvironment() != Environment.NETHER && to.getWorld().getEnvironment() != Environment.THE_END) {
       }
        int isAllowed = isAllowed(player);
        if(isAllowed == 0) {
             player.sendMessage("You may not enter the Nether! " + ControlManager.rulingnation + "is currently in control of the nether!");
            event.setCancelled(true);
        }
        else if (isAllowed == 1) {
            player.sendMessage("You must join a town (and then fight to get the most kills) to be able to enter the portal.");
            event.setCancelled(true);
       }
        else if (isAllowed == 2) {
            player.sendMessage("You are a member of the ruling town of " + ControlManager.ruling + " Congratulations, enjoy the nether!");
            
        }
        else if (isAllowed == 3) {
            player.sendMessage("You are allowed into the Nether with the kind permission of the ruling town of " + ControlManager.ruling);
        }
   
}
   
   public static int isAllowed(Player player) {
       try {
        Town town = TownyUniverse.getDataSource().getResident(player.toString()).getTown();
        Nation nation = TownyUniverse.getDataSource().getTown(town.toString()).getNation();
        if (town == ControlManager.ruling || player.hasPermission("portalcontrol.access")) {
            return 2;
        }
        else if (nation == ControlManager.rulingnation) {
            return 3;
        }
        else {
        return 0;
        }
       }
       catch (NotRegisteredException exception) {
           return 1;
       }
       }
       
      
   }
   

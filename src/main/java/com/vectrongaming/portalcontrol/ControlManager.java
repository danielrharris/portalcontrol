/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vectrongaming.portalcontrol;

import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Nation;
import com.palmergames.bukkit.towny.object.Town;
import com.palmergames.bukkit.towny.object.TownyUniverse;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 *
 * @author DanielRHarris
 */
public class ControlManager {

    public static HashMap<String, MutableInteger> towns = new HashMap<String, MutableInteger>();
    public static Town ruling;
    public static Nation rulingnation;
    private static Town tempTown;

    public static void save(File dataFolder) throws Exception {
        if (!dataFolder.exists()) {
            dataFolder.mkdir();
        }
        File f = new File(dataFolder, "score.dat");
        if (f.exists()) {
            f.delete();
        }
        f.createNewFile();

        BufferedWriter bw = new BufferedWriter(new FileWriter(f));
        for (String p : towns.keySet()) {
            bw.write(p + "," + towns.get(p).value);
            bw.newLine();
        }
        bw.flush();
        bw.close();
    }

    public static void load(File dataFolder) throws Exception {
        if (!dataFolder.exists()) {
            return;
        }
        File f = new File(dataFolder, "score.dat");
        if (!f.exists()) {
            return;
        }

        DataInputStream dis = new DataInputStream(new FileInputStream(f));
        int nTowns = dis.readInt();
        System.out.println("Reading towns: " + nTowns);
        for (int i = 1; i <= nTowns; i++) {
            String str = dis.readUTF();
            MutableInteger mi = new MutableInteger(dis.readInt());
            System.out.println("Reading " + str + " " + mi.value);
            towns.put(str, mi);

        }
    }

    public static void deleteSave() {
        File file = new File("adventurize/score.dat");
        file.delete();

    }

    private static void kickPlayers() {

        for (Player plr : Bukkit.getOnlinePlayers()) {
            if (plr.getWorld().getEnvironment().getId() == -1 && ControlListener.isAllowed(plr) != 2 || ControlListener.isAllowed(plr) != 3) {
                plr.teleport(plr.getWorld().getSpawnLocation());
                plr.sendMessage(ChatColor.GOLD + "[PortalControl] " + ChatColor.YELLOW + "You lost control of the nether!");
            }
        }
    }

    public static class MutableInteger {

        public int value;

        public MutableInteger(int v) {
            value = v;
        }

        private int intValue() {
            return value;
        }
    }

    public static void calculateScore() {
        //Get the town with the highest score from the hashmap towns and set it to tempTown

        //Get list of towns, check if town exists in map, if not, add it:
        List allTowns = TownyUniverse.getDataSource().getTowns();

        for (int i = 0; i < allTowns.size(); i++) {
            Town town = null;
            try {
                town = TownyUniverse.getDataSource().getTown(allTowns.get(i).toString());
            } catch (NotRegisteredException ex) {
                Logger.getLogger(ControlManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if (!towns.containsKey(town)) {
                towns.put(town.toString(), new MutableInteger(0));
            }
        }

        Map.Entry<String, MutableInteger> maxEntry = null;

        for (Map.Entry<String, MutableInteger> entry : towns.entrySet()) {
            if (maxEntry == null || ((MutableInteger) entry).intValue() > ((MutableInteger) maxEntry).intValue()) {
                maxEntry = entry;
            }
        }
        try {
            tempTown = TownyUniverse.getDataSource().getTown(maxEntry.getKey());
        } catch (NotRegisteredException ex) {
            Logger.getLogger(ControlManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (tempTown != ruling) {
            try {
                ruling = tempTown;
                rulingnation = TownyUniverse.getDataSource().getTown(ruling.toString()).getNation();
            } catch (NotRegisteredException exception) {
            }
            for (Player plr : Bukkit.getOnlinePlayers()) {
                plr.sendMessage(ChatColor.GOLD + "[PortalControl] " + ChatColor.YELLOW + "The town of " + ruling + " in the nation of " + rulingnation + " is now in control of the nether!");
            }
            kickPlayers();
        }
    }
}

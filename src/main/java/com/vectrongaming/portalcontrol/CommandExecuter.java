/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vectrongaming.portalcontrol;

import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Town;
import com.palmergames.bukkit.towny.object.TownyUniverse;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 *
 * @author DanielRHarris
 */
public class CommandExecuter implements CommandExecutor {
    private PortalControl plugin;
    private int townpts;

    CommandExecuter(PortalControl aThis) {
        this.plugin = aThis;
    }

    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
        if (strings.length == 0) {
            cs.sendMessage(ChatColor.GREEN + "The town currently in control of the nether is: " + ControlManager.ruling.toString() + " with " + ControlManager.towns.get(ControlManager.ruling).value + " points.");
            try {
                Town town = TownyUniverse.getDataSource().getResident(cs.toString()).getTown();

                townpts = ControlManager.towns.get(town).value;

                cs.sendMessage(ChatColor.GREEN + "Your town has a total of " + townpts + " points!");
            } catch (NotRegisteredException exception) {
            }
        }
        String farg = strings[0];
        if (farg.equals("delete")) {
			if (!cs.hasPermission("portalcontrol.admin")) {
				return false;
			}
                        ControlManager.deleteSave();
			cs.sendMessage(ChatColor.GREEN + "Scores Deleted");
        }
        return true;
    }
   
}